+++
title = "About"
description = "To become a better coder, Code.Every.Day"
date = "2019-06-10"
aliases = ["about-us","about-code-every-day","contact"]
author = "Shashank Srivastava"
featureimage = "img/code.jpg"
menu = "nav"
+++

## `Code.Every.Day` is my effort at getting better at coding.

A few days ago, I realised something. I decided to make myself a better software developer. I wanted to hone my skills by
learning as much as I can. I have coded a few simple but useful applications. Below is the list of some of my
coding projects.

 - [EMI/Income Tax Calculator](https://shashankshekharsrivastava.github.io/)
 - [My Music Stats](https://mymusicstats.github.io)

There are a few more projects that are not mature enough to be disclosed. I am still trying to find time to work on them.

But, amidst all these coding activities, I realised that I wasn't getting any better. I was creating stuff for myself, but
I wasn't feeling comfortable as a ``coder``. And there was a simple reason for it. I wasn't consistent. I was not coding regularly.
My last personal project was in September last year.

In the past nine months, I had not written much code. And this made me come to a conclusion that I need to write `Code.Every.Day`.

> *Practice alone doesn't make you better, but deliberate practice does*.

I have decided to write at-least some `Code.Every.Day`. No matter how many lines. All I need is to **`code`** & **`Code.Every.Day`**.
All my progress will be chronicled on this blog in form of a daily post.
