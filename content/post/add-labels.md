+++
author = "Shashank Srivastava"
title = "Added Labels on Homepage"
date = "2019-06-23"
description = "Started using Labels instead of plain text."
tags = [
    "html",
    "frontend",
    "uikit",
]
categories = [
    "frontend",
    "HTML",
]
+++

Changed the UI a bit by using UIKit labels instead of normal text on my Flask app. Now it looks a bit more vibrant & colorful!

Below is a snippet from the main template.

{{< highlight html >}}
<h4 class="uk-text uk-label uk-label-primary">Took Off From</h4>
{{< /highlight >}}
