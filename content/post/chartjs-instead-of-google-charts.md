+++
author = "Shashank Srivastava"
title = "Replaced Google Charts With Chart.js"
date = "2019-06-24"
description = "Replaced Google Charts with Chart.js in my EMI/Income Tax Calculator application."
tags = [
    "html",
    "javascript",
    "ui",
    "frontend",
    "charts",
]
categories = [
    "frontend",
    "HTML",
    "JavaScript",
    "UI",
]
+++

Today, I replaced Google Charts with Chartjs library in my [EMI/Income Tax Calculator](https://shashankshekharsrivastava.github.io/) application.
The previous version of my web-app was using Google Charts API but it was giving the app a dated appearance. 

While working on my other application, [I came across Chart.js](https://shashanksrivastava.gitlab.io/post/2019-06-14-use-chartjs-hugo-site/) & I liked it so much that I started using it for my other projects.

Below is the code snippet from [EMI/Income Tax Calculator](https://shashankshekharsrivastava.github.io/)

### JavaScript Function calling Chart.js

{{< highlight javascript>}}
function drawSavingsChart() {
  var providentFund = Number(document.getElementById("pf").value);
  var nationalSavingScheme = Number(document.getElementById("nsc").value);
  var publicProvidentFund = Number(document.getElementById("ppf").value);
  var mutualFunds = Number(document.getElementById("mf").value);
  var lifeInsurance = Number(document.getElementById("lic").value);
  var homeloanPrincipal = Number(document.getElementById("homeloanPrincipal").value);
  var chartData = {
    labels: ['Provident Fund', 'Public Provident Fund', 'NSC', 'Mutual Funds', 'Life Insurance Policies'],
    datasets: [{
      label: 'Rupees',
      data: [providentFund, publicProvidentFund, nationalSavingScheme, mutualFunds, lifeInsurance],
      spanGaps: false,
      backgroundColor: ["rgb(255, 99, 132)", "rgb(54, 162, 235)", "rgb(255, 205, 86)", 'rgb(66, 244, 167)', 'rgb(157, 65, 244)']
    }]
  }
  // get chart canvas
  var ctx = document.getElementById("piechartSavings");
  // create the chart using the chart canvas
  var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: chartData,
    options: {
      title: {
        display: true,
        text: 'Section 80C Breakup'
      }
    }
  });
}
{{< /highlight >}}


