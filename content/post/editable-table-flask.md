+++
author = "Shashank Srivastava"
title = "Trying To Make The Trip Table Editable"
date = "2019-06-28"
description = "Working on making my table editable."
tags = [
    "python",
    "flask",
    "mysql",
    "frontend",
    "backend",
]
categories = [
    "Python",
    "Flask",
    "Backend",
    "Frontend",
]
+++

Now that my Trip Tracker web-app is almost complete, I need to make the table which shows the trip details editable.
And since I am not using Flask Table module, there isn't an easy way to do it. I have to write additional code to edit/update
& delete the records from the table.

I didn't spend much time doing the development today. Fell asleep while coding & then didn't quite feel like it after waking up.
Managed to dedicate >30 minutes for coding, though.

### RescueTime Daily Summary for June 28, 2019

> On June 28, 2019, I logged **7h 30m** on the computer with RescueTime. 4h 44m of that time was scored as "productive" or "very productive", while 1h 49m was scored as distracting. I'm trying to limit my time on social networks (2m), and email (16m 43s). What I really want to be spending more time on is **coding**, and I managed to spend **35m 18s** on that.
