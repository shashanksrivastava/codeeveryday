+++
author = "Shashank Srivastava"
title = "Font Awesome Icons in Flask Application"
date = "2019-06-15T11:04:40+04:00"
description = "Extended my Flask application by adding Font Awesome icons"
tags = [
    "font-awesome",
    "frontend",
    "flask",
    "UI",
]
categories = [
    "frontend",
    "UI",
    "flask",
]
series = ["How To"]
aliases = ["font-awesome-icons"]
+++

Today I used [Font Awesome](https://fontawesome.com) icons on my Flask application to give it a cool look. I know it's nothing substantial
but still, it is better than not writing any code at all.

## HTML Code

{{< highlight html >}}
<!-- Font Awesome FILES -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/XXXXXX.js"></script>
{{</ highlight >}}

And then I used it as...

{{< highlight html >}}
<span class="fas fa-suitcase uk-text-primary">
{{</ highlight >}}