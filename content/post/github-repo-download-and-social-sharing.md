+++
author = "Shashank Srivastava"
title = "Added GitHub Download Button & Social Sharing Icons"
date = "2019-06-19"
description = "I added a nice looking GitHub Download button & social sharing icons to my main blog."
tags = [
    "html",
    "javascript",
    "frontend",
    "social media",
]
categories = [
    "HTML",
    "JavaScript",
    "Frontend",
]
+++

Today I added a GitHub Download button to download my GitHub repository directly from my blog & social media sharing
icons to share my posts via Facebook and Twitter.

This is how I did it.

<!--more-->

## GitHub Download button

{{< highlight html >}}
<!-- GitHub repo download button. -->
<a class="github-button" href="https://github.com/shashank-ssriva/REST-To-Do/archive/master.zip" data-size="large" aria-label="Download shashank-ssriva/REST-To-Do on GitHub">Download</a>
<script async defer src="https://buttons.github.io/buttons.js"></script>
{{< /highlight >}}

This snippet adds a nice download button to my blog. Thanks to [https://buttons.github.io/](https://buttons.github.io/) for their
awesome utility.

## Social sharing icons

Not the most efficient way. But it does the job. I might change it in coming few days, but sticking with it for now.

{{< highlight html >}}
<ul class="list-inline footer-links">
    <li>
        <a href="https://www.facebook.com/sharer/sharer.php?u=https://shashanksrivastava.gitlab.io/post/2019-06-18-create-rest-api-php-mysql&t={Create a REST API Using PHP & MySQL.}" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
    <li>
        <a href="https://twitter.com/intent/tweet?url=https://shashanksrivastava.gitlab.io/post/2019-06-18-create-rest-api-php-mysql&text=Create%20a%20REST%20API%20Using%20PHP%20%26%20MySQL%20via%20shashanksriva" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
</ul>    
{{< /highlight >}}

You can check this [post on my blog](https://shashanksrivastava.gitlab.io/post/2019-06-18-create-rest-api-php-mysql) to see the social sharing icons.
