+++
author = "Shashank Srivastava"
title = "Use HTML tags inside Bootstrap Popover"
date = "2019-06-16"
description = "I formatted my Popover data using HTML in Bootstrap."
tags = [
    "bootstrap",
    "html",
    "popover",
    "javascript",
    "tooltip",
]
categories = [
    "frontend",
    "UI",
    "HTML",
]
series = ["How To"]
aliases = ["html-content-in-bootstrap-popover"]
+++

Last year, I created [EMI/Income Tax Calculator](http://shashankshekharsrivastava.github.io/) application using pure client-side
JavaScript. I use my web-app a lot & depend on it while filing my Income Tax Returns.

Today, I was simply browsing my web-app & noticed that a few things are missing. I needed to display additional information under a few
sections. I was already using Bootstrap Popovers to display such information in other sections.

So, I went ahead and added the Popovers. But when I tried to format the data using HTML tags inside it, I saw that it didn't render
any tag.

{{< highlight html >}}
<input data-content="An additional <b>50,000 Rupees</b> can be invested under NPS">
{{< /highlight >}}

I went to the Bootstrap's website to see what was I missing. There I found that I had to use `data-html="true"` to enable HTML tags.
When I added it, I was able to format my data.

<!--more-->


## After

{{< highlight html >}}
<input data-html="true" data-content="An additional <b>50,000 Rupees</b> can be invested under NPS">
{{< /highlight >}}

Below is what it looks like.

![Popover showing the HTML formatted data]
(/codeeveryday/img/bootstap-popover.PNG)
