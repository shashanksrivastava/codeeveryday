+++
author = "Shashank Srivastava"
title = "More self In Python"
date = "2019-06-30"
description = "Finally I have a much better understanding of <code>self</code> in Python."
tags = [
    "python",
    "self",
]
categories = [
    "Python",
]
+++

I have been struggling a bit with `self` in Python for a few days. But today I managed to crack it.
I now understand it much better & have quite a better clarity on how `self` works in Python.

This is a simple code that I wrote to unveil the mystery. Will explain it on my other blog.

### Python code

{{< highlight python >}}
"""
__init__ is called when ever an object of a class is created.
"""


class Planets:
    def __init__(self, moons, mass, rad):
        self.moons = moons
        self.mass = mass
        self.rad = rad
        print("An object from Planet class has been created!\n")


earth = Planets(2,  5972000000000000000000, 6371)
print("The Earth has %s moons." % earth.moons)
print("The Earth's mass is %s tonnes." % earth.mass)
print("The Earth's radius is %s Kms.\n" % earth.rad)

jupiter = Planets(79,  "1.898 × 10^24", 69911)
print("Jupiter has %s moons." % jupiter.moons)
print("Jupiter's mass is %s tonnes." % jupiter.mass)
print("Jupiter's radius is %s Kms.\n" % jupiter.rad)

print("Jupiter is %s times bigger than Earth." % (jupiter.rad / earth.rad))
{{< /highlight >}}

### Output

{{< highlight bash >}}
root@shashank-mbp /p/v/root# /usr/local/opt/python/bin/python3.7 /Users/admin/Downloads/planets.py
An object from Planet class has been created!

The Earth has 2 moons.
The Earth's mass is 5972000000000000000000 tonnes.
The Earth's radius is 6371 Kms.

An object from Planet class has been created!

Jupiter has 79 moons.
Jupiter's mass is 1.898 × 10^24 tonnes.
Jupiter's radius is 69911 Kms.

Jupiter is 10.973316590802073 times bigger than Earth.
root@shashank-mbp /p/v/root# 
{{< /highlight >}}