+++
author = "Shashank Srivastava"
title = "Showing the Amount Entered in Words As Well"
date = "2019-06-26"
description = "Showing the amount entered in words as well now. Makes it easy & convenient."
tags = [
    "html",
    "frontend",
    "bootstrap",
]
categories = [
    "frontend",
    "HTML",
]
+++

As discussed in the [last post](https://shashanksrivastava.gitlab.io/codeeveryday/post/removed-google-plus/), I have added
the feature which automatically display the amount being entered in words. For someone like me, it causes great inconvenience
when I have to enter any amount upward of 4-5 digits. 

Now I can easily enter the amount & [my web-app](https://shashankshekharsrivastava.github.io/) will automatically display the same in words.

### JavaScript snippet

{{< highlight javascript >}}
function totalIncomeInWordsFn() {
  var totalIncomeInWords = numberToWords.toWords(Number(document.getElementById("totalIncome").value));
  document.getElementById("totalIncomeInWords").innerHTML = totalIncomeInWords;
}
{{< /highlight >}}

### HTML snippet

{{< highlight html >}}
<label for="totalIncome" class="font-weight-bold">Total Income</label>
<input type="number" class="form-control" id="totalIncome" placeholder="Gross Income" required
onkeyup="enableCalcBtn(), totalIncomeInWordsFn()">
<span id="totalIncomeInWords" class="form-text text-danger"></span>
{{< /highlight >}}

Below is the code in action.

![Number to words]
(/codeeveryday/img/number-to-words.PNG)