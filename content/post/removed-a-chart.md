+++
author = "Shashank Srivastava"
title = "Removed A Chart"
date = "2019-06-29"
description = "Removed an unneccessary chart from my Travel Tracker web-app."
tags = [
    "html",
    "frontend",
]
categories = [
    "HTML",
]
+++

Wasn't a really productive day, per se. Didn't get much time to work on my code.
But I managed to touch-up my Travel Tracker web-app a bit by removing a chart
which wasn't needed. Nothing substantial, but something is better than nothing.

Looking forward to more exciting coding activities.