+++
author = "Shashank Srivastava"
title = "Removed Google Plus Reference"
date = "2019-06-25"
description = "Removed Google Plus reference from my web-app."
tags = [
    "html",
    "frontend",
    "bootstrap",
]
categories = [
    "frontend",
    "HTML",
]
+++

Now that Google Plus is no longer available, I have removed all the references to it from my [EMI/Income Tax Calculator](https://shashankshekharsrivastava.github.io/) application.

Also, I worked on displaying the **amount of Rupees in words** as one enters the value in respective text-boxes under Income Tax section.
The [EMI calculator section](https://shashankshekharsrivastava.github.io/) already has this feature.

Will finish it today.
