+++
author = "Shashank Srivastava"
title = "Created A REST API"
date = "2019-06-18"
description = "I wrote a REST API using PHP/MySQL."
tags = [
    "php",
    "mysql",
    "rest-api",
]
categories = [
    "backend",
    "REST",
    "PHP",
]
series = ["How To"]
aliases = ["rest-api-in-php"]
+++

Today I worked on creating a REST API in `PHP` 7 (*PHP 7.1.23*) using `MySQL` 8 (*MySQL 8.0.16*) as the database.
This API allows creating new **To-Do** items & retrieving them from the database via REST clients, such as
`cURL`, browsers & Postman.

The entire tutorial is available on my [main blog](https://shashanksrivastava.gitlab.io/post/2019-06-18-create-rest-api-php-mysql/) & the code can be found on my [GitHub repository](https://github.com/shashank-ssriva/REST-To-Do). You can also download it by
clicking the button below.

<!-- GitHub repo download button. -->
<a class="github-button" href="https://github.com/shashank-ssriva/REST-To-Do/archive/master.zip" data-size="large" aria-label="Download shashank-ssriva/REST-To-Do on GitHub">Download</a>
<script async defer src="https://buttons.github.io/buttons.js"></script>

<!--more-->

## `GET` Request

{{< highlight bash >}}
{
    https://localhost/~admin/REST-TO-DO/info?task=Write Code
}
{{< /highlight >}}

Yes, my end-point is HTTPS enabled & also, it runs on TLS 1.2 only. More information on it is available on my tutorial.

## Postman output/Response

{{< highlight json >}}
{
    "status": 1,
    "info": [
        {
            "Task": "Write Code",
            "Date": "18/06/2019",
            "Priority": "1"
        }
    ]
}
{{< /highlight >}}