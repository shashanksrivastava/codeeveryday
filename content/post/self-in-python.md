+++
author = "Shashank Srivastava"
title = "self In Python"
date = "2019-06-27"
description = "Learned a bit about <code>self</code> In Python."
tags = [
    "python",
    "self",
]
categories = [
    "Python",
]
+++

Tried to wrap my head around `self` in Python. Haven't really understood it fully & I still need some more clarity
before I can safely assume I know how to use it more in my codes.

Below is a simple Python script which I have taken from a Quora answer. I will replace it with my own code once I
get a hang of it.

### Sample Python code

{{< highlight python >}}
class Student(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age


class School(object):
    students = []

    def __init__(self, name):
        self.name = name

    def add_student(self, student):
        self.students.append(student)

    def show_students(self):
        print("{0} Student Roster".format(self.name))
        for s in self.students:
            print("{0}: {1}".format(s.name, s.age))


my_school = School("My University")
first_student = Student("John Doe", 20)
second_student = Student("Brian Doe", 21)
my_school.add_student(first_student)
my_school.add_student(second_student)
my_school.show_students()
{{< /highlight >}}

### Output

{{< highlight bash >}}
root@shashank-mbp /U/a/D/codeeveryday# /usr/local/opt/python/bin/python3.7 /Users/admin/Downloads/self.py
My University Student Roster
John Doe: 20
Brian Doe: 21
root@shashank-mbp /U/a/D/codeeveryday# 
{{< /highlight >}}