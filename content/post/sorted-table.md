+++
author = "Shashank Srivastava"
title = "Sorted the Trips Details Table"
date = "2019-06-22"
description = "Sorted the table by date in descending order & made the UI uniform across both the pages."
tags = [
    "html",
    "mysql",
    "ui",
    "frontend",
    "backend",
]
categories = [
    "frontend",
    "HTML",
    "MySQL",
    "UI",
]
+++

Today, I sorted my **Trips Details** table in descending order & also applied the same UI to both the pages so that
the entire app has a uniform look.
{{< highlight python>}}
cursor.execute("SELECT * FROM TravelDetails ORDER BY travel_date DESC;")
{{< /highlight >}}

Also edited the footer so that it shows my name.

{{< highlight html >}}
<p class="uk-text-small uk-text-center">(C) Copyright 2019 - Created by <a
href="https://shashanksrivastava.gitlab.io"> Shashank Srivastava</a> | Built with <a
	href="https://getuikit.com" title="Visit UIkit 3 site" target="_blank" data-uk-tooltip>
    <span data-uk-icon="uikit"></span></a>
</p>
{{< /highlight >}}
