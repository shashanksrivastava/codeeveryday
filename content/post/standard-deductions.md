+++
author = "Shashank Srivastava"
title = "Added Standard Deductions to my Income Tax Calculator Web-app"
date = "2019-06-17"
description = "I added Standard Deduction to my Income Tax Calculator web-app, which was reintroduced last year. It means, my web-app can now be used to calculate Income Tax for the last financial year as well."
tags = [
    "html",
    "javascript",
    "frontend"
]
categories = [
    "frontend",
    "UI",
    "HTML",
    "JavaScript",
]
series = ["General"]
aliases = ["generic-html-javascript"]
+++

Today I updated my [EMI/Income Tax Calculator](http://shashankshekharsrivastava.github.io/) application to reflect the changes announced
by the Finance Minister of India last year.

<!--more-->

Travel & Medical allowances have been replaced with a Standard Deduction of Rupees 40,000 & I updated my web-app accordingly.

## HTML code

{{< highlight html >}}
<div class="col-auto">
    <label for="conveyance" class="font-weight-bold">Standard Deduction (reintroduced last year)</label>
    <input type="number" class="form-control" id="standard-deduction" placeholder="Standard Deduction" readonly
    value="40000" data-toggle="popover" data-trigger="focus" data-html="true" data-placement="top" title="Important Information" data-content="Standard Deduction of <b>Rupees 40,000</b> was
    reintroduced last year <i>in place of transport and medical allowance</i>. This amount will be subtracted from the Gross Income." >
</div>
{{< /highlight >}}


## JavaScript code

{{< highlight javascript >}}
var standardDeduction = Number(document.getElementById("standard-deduction").value);
var netIncome = (grossIncome - houseRentAllowance - conveyanceAllowance - standardDeduction);
{{< /highlight >}}
