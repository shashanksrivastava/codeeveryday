+++
author = "Shashank Srivastava"
title = "Struggle With Disqus Continued..."
date = "2019-06-21"
description = "Disqus comment system still didn't work."
tags = [
    "html",
    "frontend",
    "javascript",
]
categories = [
    "frontend",
    "HTML",
    "JavaScript",
]
+++

Disqus is still not loading on my blog. I have tried everything to make it work but it still fails to load.

Yet another day when I don't have anything substantial to show. But the good thing is that I tried & spent quite some time
coding. Below is the time I spent on Software Development activities today.

![Software Development activities]
(/codeeveryday/img/software-development-21-june.png)

**More than three hours on coding!**
