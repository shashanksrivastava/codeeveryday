+++
author = "Shashank Srivastava"
title = "Tried Integrating Disqus"
date = "2019-06-20"
description = "Trying to connect my blog with Disqus comment system."
tags = [
    "html",
    "frontend",
    "javascript",
]
categories = [
    "frontend",
    "HTML",
    "JavaScript",
]
+++

Today I started working on integrating Disqus with my blog but couldn't set it up successfully. It fails to load
& I can't seem to figure out why.

Not a very productive day, per se. There isn't *much to show* except the below screenshot which displays the time I
spent on Software Development activities.

<!--more-->

![Software Development activities]
(/codeeveryday/img/software-development-activity-20-june.png)

You can see that I spent more than an hour on coding.
