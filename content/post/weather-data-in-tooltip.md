+++
author = "Shashank Srivastava"
title = "Display Weather Data In Tooltip"
date = "2019-06-14"
description = "Using the parsed weather data, I displayed the relative humidity & weather information in the tooltip."
tags = [
    "python",
    "json",
    "frontend",
    "flask",
    "tooltip",
]
categories = [
    "frontend",
    "data parsing",
    "flask",
]
series = ["How To"]
aliases = ["dynamic-data-tooltip"]
+++

Yesterday I had fetched the weather data using Python. Today I used that data to display in a tooltip
upon hovering the cursor over the city's name.

My code consists of two parts, frontend & backend. The backend Python script parses the weather data from an API
and stores the values in two variables `most_visited_city_rh` and `most_visited_city_temp`.

These two variables represent **relative humidity** & **current temperature** respectively.

The frontend (*Flask template*) displays the most travelled International & Domestic destinations & when I hover the cursor over these, a tooltip shows the current temperature & relative humidity.
<!--more-->

## Python Code

{{< highlight python >}}
with urllib.request.urlopen("https://api.weatherbit.io/v2.0/current?city=%s&key=XXXXXXXX" %mostvisiteddest) as url:
    most_visited_city_data = json.loads(url.read().decode())
    most_visited_city_temp = most_visited_city_data['data'][0]['temp']
    most_visited_city_rh = most_visited_city_data['data'][0]['rh']

return render_template('index.html', most_visited_city_temp=most_visited_city_temp, most_visited_city_rh=most_visited_city_rh)            
{{< /highlight >}}

## HTML Template

{{< highlight html >}}
<h1 class="uk-heading-primary uk-margin-remove uk-text-primary"
		uk-tooltip="title: <i class='fas fa-cloud-sun'></i> Current humidity in {{ mostvisiteddest }} : {{ most_visited_city_rh }} %. <br><i class='fas fa-sun'></i> Current temperature in {{ mostvisiteddest }} : {{ most_visited_city_temp }} degree.; pos: bottom">{{ mostvisiteddest }}
</h1>
{{< /highlight >}}

Below is the screenshot of my code in action.

![Tooltip showing weather information]
(/codeeveryday/img/tooltip.png)
