+++
author = "Shashank Srivastava"
title = "Weather Data Using Python"
date = "2019-06-13"
description = "I parsed JSON data file containing weather information using Python."
tags = [
    "python",
    "json",
]
categories = [
    "backend",
    "data parsing",
]
series = ["How To"]
aliases = ["parse-json-file-python"]
+++

I am working on a small pet project (*a web-app*) in Python for which I need to work with weather APIs. So, today I wrote a small piece
of Python code that helps me retrieve the relative humidity & temperature of a given city from a JSON file.

This data will now be fed to the front-end of my web-app. I am thinking to display it inside a tool-tip.
<!--more-->

## Python Code

{{< highlight python >}}
import json
import requests
with open("C:\\Users\\Shashank\\Downloads\\current-weather.json", 'r') as f:
    weather_dict = json.load(f)
    
rh = weather_dict['data'][0]['rh']
temp = weather_dict['data'][0]['temp']

print(rh)
print(temp)
{{< /highlight >}}

## Output

{{< highlight batch >}}
C:\Users\Shashank>C:/Users/Shashank/AppData/Local/Programs/Python/Python37-32/python.exe c:/Users/Shashank/Downloads/weather.py
24
38.1
{{< /highlight >}}
